# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------


import sys, socket, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""ss server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port
llistaPeers=[]          # VARIABLE GLOBAL DEBIDO A QUE LO HACEMOS FUERA DE LAS FUNCIONES

#-------------------------------------

def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)



s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:     # PLEGAREMOS GRACIAS A QUE LE PASAMOS UNA SEÑAL
    conn, addr = s.accept()
    print("Connected by", addr)     # NOS PREGUNTARÁ EN EL EXAMEN QUE SOLO NOS MUESTRE EN CASO DE QUE EL USUARIO PASE UNA "-v" DE DEBUG
    llistaPeers.append(addr)
    command = "ss -ltn"
    pipeData = Popen(command,stdout=PIPE)
    
    for line in pipeData.stdout:
      conn.send(line)
    conn.close()



