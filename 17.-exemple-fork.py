#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-fork.py
#
# --------------------------------------

import sys, os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())


pid=os.fork()       # BIFURCA A PARTIR DEL RETORN DE os.fork, TENEMOS UN PID Y OTRO CON UN 0
if pid != 0:
   os.wait()
   print("Programa pare: ",os.getpid(), pid)
else:
   print("Programa fill: ", os.getpid(), pid)           # EN CASO DE QUE SEA 0
   while True:
    pass
print("Hasta luego Lucas!")
sys.exit(0)



# os.fork() -> CREACIÓN DE UN NUEVO PROCESO. EL PROCESO PADRE RECIBE DEL PROCESO DEL HIJO os.fork(), Y EL HIJO RECIBE EL PROCESO 0
