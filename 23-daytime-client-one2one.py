# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Gener 2024
# -------------------------------------
import sys,socket, argparse
parser = argparse.ArgumentParser(description="""Client """)
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
# ---------------------------------------------------------
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))
while True:
  data = s.recv(1024)
  if not data: break
  print('data', repr(data))
s.close()
sys.exit(0)


 # EMPIEZA A ESCUCHAR HASTA QUE PLEGAMOS, LA DIFERENCIA ENTRE EL PROGRAMA ANTERIOR ES QUE DECIMOS CON QUE PUERTO DEJA DE CONECTAR


# nc -l 60001