#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-fork.py
#
# --------------------------------------

import sys,os,signal

pid=os.fork()

if pid !=0:
  #os.wait()
  print("Programa pare", os.getpid(), pid)
  sys.exit(0)


# PROGRAMA HIJO
  
os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls","/"])     # LO QUE HACE ES TRANSFORMAR(SUBSTITUIR) UN PROPIO PROCESO POR UNO NUEVO


# NO SE EJECUTARÁ DEBIDO A QUE EL PROCESO ACTUAL QUEADARÁ SUBSTITUIDO POR "ls"
print("Hasta luego Lucas!")         

sys.exit(0)



# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 19-exemple-execv.py 
# Programa pare 14830 14831
# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ total 68
#  0 lrwxrwxrwx   1 root root     7 jun 10  2023 bin -> usr/bin
#  4 drwxr-xr-x   5 root root  4096 nov  6 09:06 boot
#  0 drwxr-xr-x  18 root root  3600 abr 25 09:10 dev
# 12 drwxr-xr-x 153 root root 12288 abr 25 11:18 etc
#  4 drwxr-xr-x   2 root root  4096 mar 21 12:08 Flags
#  4 drwxr-xr-x   5 root root  4096 sep 21  2023 home
#  0 lrwxrwxrwx   1 root root    30 nov  6 09:06 initrd.img -> boot/initrd.img-6.1.0-12-amd64
#  0 lrwxrwxrwx   1 root root    29 nov  6 09:06 initrd.img.old -> boot/initrd.img-6.1.0-9-amd64
#  0 lrwxrwxrwx   1 root root     7 jun 10  2023 lib -> usr/lib
#  0 lrwxrwxrwx   1 root root     9 jun 10  2023 lib32 -> usr/lib32
#  0 lrwxrwxrwx   1 root root     9 jun 10  2023 lib64 -> usr/lib64
#  0 lrwxrwxrwx   1 root root    10 jun 10  2023 libx32 -> usr/libx32
# 16 drwx------   2 root root 16384 jun 14  2023 lost+found
#  4 drwxr-xr-x   3 root root  4096 sep 19  2023 media
#  4 drwxr-xr-x   2 root root  4096 jun 10  2023 mnt
#  4 drwxr-xr-x   9 root root  4096 mar 21 12:08 opt
#  0 dr-xr-xr-x 331 root root     0 abr 25 09:10 proc
#  4 drwx------  28 root root  4096 sep 20  2023 root
#  0 drwxr-xr-x  38 root root  1160 abr 25 09:43 run
#  0 lrwxrwxrwx   1 root root     8 jun 10  2023 sbin -> usr/sbin
#  4 drwxr-xr-x   2 root root  4096 jun 10  2023 srv
#  0 dr-xr-xr-x  13 root root     0 abr 25 09:10 sys
#  0 drwxrwxrwt  23 root root   640 abr 25 10:36 tmp
#  4 drwxr-xr-x  14 root root  4096 jun 10  2023 usr
#  4 drwxr-xr-x  11 root root  4096 jun 10  2023 var
#  0 lrwxrwxrwx   1 root root    27 nov  6 09:06 vmlinuz -> boot/vmlinuz-6.1.0-12-amd64
#  0 lrwxrwxrwx   1 root root    26 nov  6 09:06 vmlinuz.old -> boot/vmlinuz-6.1.0-9-amd64
# 
