#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce 
# M06 Curs 2023-2024
# popen-sql.py
# --------------------------------------


import sys, argparse
from subprocess import Popen, PIPE
# -------------------------------
command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)              # SIGNIFICA QUE ESTE SUBPROCESO ESCUCHARÁ Y ESCRIBIRÁ EN EL PIPE -> popen() -> iniciamos subproceso
pipeData.stdin.write("select * from oficinas;\n\q\n")           # LO QUE HACEMOS ES DARLE AL ESPACIO DESPUES HACER LA ORDEN \q Y DARLE OTRA VEZ EL ESPACIO
for line in pipeData.stdout:
    print(line,end="")    
exit(0)

