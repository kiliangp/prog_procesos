#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Kilian Steven Guanoluisa Pionce
#
# ----------------

import argparse

# CREAMOS LA VARIABLE "parser"
# Constructor -> CUANDO LA FUNCIÓN COMIENZA EN MAYÚSCULA

# DEFINIMOS PROPIEDADES (description, prog, epilog)
parser = argparse.ArgumentParser(    
    description="programa d'exemple d'arguments",\
    prog="02-arguments.py",\
    epilog="hasta luego lucas!")

parser.add_argument("-n","--nom", type=str,\
    help="nom usuari") # DEFINIMOS UN ARGUMENTO A PROCESAR

parser.add_argument("-e","--edat", type=int,\
    dest="userEdat", help="edat a processar",\
    metavar="edat")



args = parser.parse_args()

print(args)
print(args.userEdat,args.nom)   # ESTÁN DENTRO DE args, POR LO QUE LO TENEMOS QUE ESPECIFICAR
exit(0)