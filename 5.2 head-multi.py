# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] file[...]
#  10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")

parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="5|10|15", choices=[5,10,15],default=10)

parser.add_argument("fileList", \
  help="fitxer a processar (stdin)", metavar="file", \
  nargs="*")

parser.add_argument("-v", "--verbose",action="store_true",default=False)

args=parser.parse_args()
print(args)

# -------------------------------------------------------

MAXLIN=args.nlin

def headFile(fitxer):
  fileIn=open(fitxer,"r")
  counter=0
  for line in fileIn:
    counter+=1
    print(line, end='')
    if counter==MAXLIN: break
  fileIn.close()

if args.fileList:   # MIRA LA LISTA ESTÁ VACÍA -> MANERA DE VALIDAR
  for fileName in args.fileList:
        if args.verbose: 
           print("\n",fileName, 40*"-")
        headFile(fileName)

exit(0)




# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 5.2\ head-multi.py -n 5 -v 1.2.abrir_fichero_arg.py 1.abrir_ficheros.py 
# Namespace(nlin=5, fileList=['1.2.abrir_fichero_arg.py', '1.abrir_ficheros.py'], verbose=True)
# 
#  1.2.abrir_fichero_arg.py ----------------------------------------
# #! /usr/bin/python3
# #-*- coding: utf-8-*-
# #
# # Kilian Steven Guanoluisa Pionce
# #
# 
#  1.abrir_ficheros.py ----------------------------------------
# #! /usr/bin/python3
# #-*- coding: utf-8-*-
# #
# # Kilian Steven Guanoluisa Pionce
# #
# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 5.2\ head-multi.py -n 5 -v 
# Namespace(nlin=5, fileList=[], verbose=True)
# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 5.2\ head-multi.py -n 5 
# Namespace(nlin=5, fileList=[], verbose=False)
# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ 

