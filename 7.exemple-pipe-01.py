#! /usr/bin/python3
#-*- coding: utf-8-*-
# Kilian Steven Guanoluisa Pionce
#
# ------------------------------------

import sys
from subprocess import Popen, PIPE # QUEREMOS SOLO IMPORTAR POPEN Y PIPE -> DE LA LIBRERÍA "SUBPROCESS" ´
                                    # SIEMPRE LO PODEMOS HACER SI NO TENEMOS PROBLEMAS CON LOS NOMBRES DE LOS PAQUETES

# ------------------------------------

command = [ "ls","/patata","/opt" ]         # LA ORDEN "WHO" SE EJECUTARÁ Y LA ÓRDEN SERÁ PASADA COMO ARGUMENTO PARA Popen
pipeData = Popen(command, stdout=PIPE)      # Popen -> constructor -> porque empieza por mayúsculas
                                                # stdout = PIPE -> la salida de WHO se va para el pipe (tubo)

for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit(0)