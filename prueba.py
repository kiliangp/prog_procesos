import os

print("Inicio del programa")

pid = os.fork()

if pid == 0:
    # Este código solo se ejecuta en el proceso hijo
    print("Soy el hijo")
else:
    # Este código solo se ejecuta en el proceso padre
    print("Soy el padre, el PID de mi hijo es", pid)

print("Fin del programa")

