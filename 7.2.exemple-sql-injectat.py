#! /usr/bin/python3
#-*- coding: utf-8-*-
# Kilian Steven Guanoluisa Pionce
#
# programa.py ruta
# ------------------------------------

import sys, argparse
from subprocess import Popen, PIPE


#============= Definició d'arguments (description, prog, epilog) ==============


parser = argparse.ArgumentParser(  
  description="""exemple Popen""")



args = parser.parse_args()




#============= Programa ==============


# Ordre que processa el sistema.
command = [ "psql -qtA -F',' -h localhost -U postgres training -c \"SELECT * FROM oficinas; \"" ]


# Defineix una variable que processa la comanda ("who").
# Qualsevol cosa en majuscula és un contructor.
pipeData = Popen(command, shell=True, stdout=PIPE)          # ESTAMOS DEFINIENDO EL TUBO -> EL SUBPROCESO


# Agafa totes les línies que li arriben del túnel(pipe).
for line in pipeData.stdout:        # Para pasarle órdenes al bucle, utilizaremos un WRITE
   print(line.decode("utf-8"),end="")
exit(0)