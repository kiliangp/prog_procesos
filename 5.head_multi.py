#! /usr/bin/python3
#-*- coding: utf-8-*-
# Kilian Steven Guanoluisa Pionce
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] [-f file]
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")

parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
       metavar="5|10|15", choices=[5,10,15],default=10)

parser.add_argument("-f","--fit",type=str,\
        help="fitxer a processar", metavar="file",\
        dest="fileLlista", action="append")

# ES INCOMPATIBLE PONER UN DEFAULT YA QUE LA OPCIÓN action='append' ES UNA ARRAY, Y DEFAULT ES UNA STRING



# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 5.head_multi.py -f file1 -f file2 -f patata -n 5
# Namespace(nlin=5, fitxer=['file1', 'file2', 'patata'])
