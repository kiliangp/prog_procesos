#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-server.py
#
# --------------------------------------


import sys,socket
from subprocess import Popen, PIPE
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1) 
conn, addr = s.accept()     # ESCUCHA HASTA QUE SE CONECTE ALGUIEN
print("Connected by", addr)  
command = ["date"]
pipeData = Popen(command,stdout=PIPE)           # PARA HACER EL CONSTRUCTOR, LO QUE QUEREMOS HACER ES ENVIAR ES INFORMACIÓN, POR LO QUE PONDREMOS "stdout" = PIPE

for line in pipeData.stdout:
  conn.send(line)
conn.close()

sys.exit(0)




