#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Kilian Steven Guanoluisa Pionce
#
# head [-n nlin] [-f file]
# default=10, file o stdin
#
# ----------------

import sys
import argparse

parser = argparse.ArgumentParser(
    description="""Mostrar les N primeres líneas""",\
    epilog="Thats all folks"
)

parser.add_argument("-n","--nlin",type=int,\
    help="Números de líneas (10)", dest="nlin",\
    metavar="numLinees",default=10)

parser.add_argument("-f", "--fit,")


# -----------------

MAX=args.nlin

fileName = "dades.txt"

# ABRIR EL ARCHIVO
with open(fileName, "r") as fileIn:
    line_count = 0

    for line in fileIn:
        print(line, end="")
        line_count += 1

        if line_count == MAX:
            break

# CERRAR EL ARCHIVO
fileIn.close()
    

exit(0) # SALIMOS DEL PROGRAMA
