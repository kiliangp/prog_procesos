#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-fork.py
#
# --------------------------------------


import sys,os,signal

def myuser1(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Hola radiola!")

def myuser2(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Aedeu andreu!")  
  sys.exit(0)

# -------------------------------------
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  #os.wait()
  print("Programa pare", os.getpid(), pid)
  print("Hasta luego lucas!")
  sys.exit(0)

# Codi del programa fill
print("Programa fill", os.getpid(), pid)
signal.signal(signal.SIGUSR1, myuser1)
signal.signal(signal.SIGUSR2, myuser2)
while True:
  pass      
print("Hasta luego lucas!")
sys.exit(0)



