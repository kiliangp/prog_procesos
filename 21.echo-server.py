#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-server.py
#
# --------------------------------------


import sys, socket
HOST = ''       # VARIABLE QUE ESTÁ DEFINIENDO EL HOST, DEBIDO A QUE ES UNA VARIABLE VACÍA, ADOPTA EL LOCALHOST -> IMPORTANTE
PORT = 50001     # UTILIZAMOS ESTE PUERTO

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       # CREA UN OBJETO DE TIPO SOCKET
                                                                # AF_INET -> ESTAMOS TRABAJANDO CON PROTOCOLES ESTÁNDARES DE CONEXIÓN
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


# LO QUE ESTAMOS HACIENDO POR ASÍ DECIRLO ES "UNA LLAMADA TELEFÓNICA"

# PRIMERA DIRECTIVA Y LA MÁS IMPORTANTE
    # TENEMOS QUE DECIRLE POR DÓNDE ESCUCHAR -> BIND (LISTEN)
s.bind((HOST,PORT))         # FORMA UN ARGUMENTO QUE ES UNA TUPLA
s.listen(1)
conn, addr = s.accept()      # SE QUEDA CLAVADO ESPERANDO A ACEPTAR UNA CONEXIÓN ENTRANTE
                                # LO QUE NOS REGRESA ES UNA TUPLA
print("Conn: ", type(conn), conn)       # REPRESENTA UNA CONEXIÓN. s -> socket -> será la "oreja" que estará escuchando 
                                            # conn -> REPRESENTA UNA CONEXIÓN YA ESTABLECIDA
print("Connected by:", addr)        

while True:
    data = conn.recv(1024)

    if not data: break  # NO SIGNIFICA QUE NO HAYAN DATOS, SIGNIFICA SI HAN COLGADO EL TELÉFONO
    conn.send(data)
    print(data)

# NO SE HARÁ NUNCA
conn.close()
sys.exit(0)


