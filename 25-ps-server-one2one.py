#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# daytime-server.py
# --------------------------------------

# IMPORTACIÓN DE LIBRERÍAS
import sys,socket,os,signal,argparse, time
from subprocess import Popen, PIPE


parser = argparse.ArgumentParser(description="""ps server""")              # EMPEZAMOS A DEFINIR LOS ARGUMENTOS QUE QUEREMOS UTILIZAR EN NUESTRO PROGRAMA CON LA LIBRERÍA ARGPARSE
parser.add_argument("-p","--port", type=int, default=50001)             # LE DEFINIMOS QUE EL ARGUMENTO PUEDE SER OPCIONAL (-p/--port), QUE TINE QUE SER TYPE -> INT, Y QUE EL PUERTO POR DEFECTO ES 50001
args = parser.parse_args()              # AÑADIMOS TODOS LOS ARGUMENTOS DEL PROGRAMA EN UNA VARIABLE
HOST = ''           # INDICAMOS QUE NO HAY HOST
PORT = args.port                # LE AÑADIMOS A LA VARIABLE 'PORT' LOS ARGUMENTOS DE PUERTO
llistaPeers=[]          # DEFINIMOS UNA LISTA
# ---------------------------------

# DEFINIMOS FUNCIONES PARA GOVERNAR EL PROGRAMA DEL SERVIDOR POR SEÑALES

def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)



# ---------------------------------

pid=os.fork()           # CREA UN NUEVO PROCESO HIJO DUPLICANDO EL PROCESO PADRE EN EJECUCIÓN, RETORNA 2 VECES, PROCESO PADRE, Y PROCESO HIJO

if pid !=0:         # SI PID NO ES 0, ESTAMOS EN EL PROCESO PADRE
  print("Server PS:", pid)      # MOSTRAMOS LA SIGUIENTE LÍNEA Y MOSTRAMOS EL NÚMERO DE PROCESO PADRE
  sys.exit(0)           # CIERRA


# LLAMAMOS A LAS FUNCIONES
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)           # CREACIÓN NUEVO SOCKET -> DEFINIMOS IPv4 (AF_INET) Y TCP (SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)           # ,1 -> ACTIVA EL SO_REUSEADOR
                                                                    # SOL_SOCKET -> ESPECIFICAMOS QUE LAS OPCIONES QUE QUEREMOS UTILIZAR SE APLIQUEN SOLAMENTE PARA SOCKET
                                                                # SO_REUSEADOR -> PERMITE REUTILIZAR DIRECCIONES LOCALES EN EL SOCKET
s.bind((HOST,PORT))     # ENLAZA EL HOST Y EL PUERTO QUE LE HEMOS ESPECIFICADO
s.listen(1)
# bucle
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    llistaPeers.append(addr)
    command="ps ax"
    # name
    fitxerNom = "/tmp/%s-%s-%s.log"%(addr[0], addr[1], time.strftime("%Y%m%d-%H%M%s"))          #%s -> string; %d -> enter  -> t2="%s %d %s" % ("pere", 33, "mama") -> es lo que hemos hecho en el ejercicio
    # obrir
    fitxer = open(fitxerNom, "w")
    while True:     # MIEMTRAS HAYA INFORMACIÓN
        data = conn.recv(1024)
        if not data: break
        fitxer.write(str(data))
    fitxer.close()  # CERRAMOS FICHERO
    conn.close()    # CERRAMOS CONEXIÓN
    





# -------------------------------
# FUNCIONAMIENTO
#     
# Server:
# while True	-> POR CADA CLIENTE
#   accepta
#   añadir a lista clientes
#   generar el nombre
#   abrir el fichero
#       While True 		-> Mientras haya datos, dejará de haber datos cuándo ya no haya líneas
#           recibir datos
#           if not data break:
#   Cerrar fichero
#   Cerrar cliente


# a221587kg@i03:~/2ASIX/M06/UF2/TEMA1/ipc/prog_procesos$ python3 25-ps-server-one2one.py 
# Server PS: 9782







