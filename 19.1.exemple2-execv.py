#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Kilian Steven Guanoluisa Pionce M06 Curs 2023-2024
# exemple-fork.py
#
# --------------------------------------

import sys,os,signal

pid=os.fork()

if pid !=0:
  #os.wait()
  print("Programa pare", os.getpid(), pid)
  sys.exit(0)


# PROGRAMA HIJO
  
# os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"])# Substitueix el propi process per un nou process (Tindrà el mateix PID). Es posen com a un array de python
# os.execl("/usr/bin/ls", "/usr/bin/ls", "-la", "/")# Es posen un a un separats per coma.
# os.execlp("ls", "ls", "-la", "/")     # NO HACE FALTA INDICAR LA RUTA PORQUE YA LA COGE DE LAS ÓRDENES.
# os.execvp("uname", ["uname", "-a"])           # EJECUTARÁ LA ÓRDEN UNAME DE LA RUTA DE UNAME
# os.execv("/bin/bash", ["/bin/bash", "nom.sh"])    # EJECUTARÁ A PARTIR DEL PROGRAMA /bin/bash EL ARGUMENTO "nom.sh"


# os.execle("/bin/bash","/bin/bash", "nom.sh", {"nom":"joan", "edat":"25"})

# NO SE EJECUTARÁ
print("Hasta luego Lucas")
sys.exit(0)