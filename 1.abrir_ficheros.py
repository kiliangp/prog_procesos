#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Kilian Steven Guanoluisa Pionce
#
# ----------------

import sys
MAX=5

fileName = "dades.txt"

# ABRIR EL ARCHIVO
with open(fileName, "r") as fileIn:
    line_count = 0

    for line in fileIn:
        print(line, end="")
        line_count += 1

        if line_count == MAX:
            break

# CERRAR EL ARCHIVO
fileIn.close()
    

exit(0) # SALIMOS DEL PROGRAMA
