#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @kiliangp M06 Curs 2023-2024
#
# signal-exemple.py
#
# Assignar un handler al senyal
#
# -------------------------------------
import sys, os, signal

def myhandler(signum, frame):            # ALGUNAS VARIABLES VIENEN PREDEFINIDAS, YA QUE ESTAMOS MANIPULANDO SEÑALES, PRIMERO ES EL NÚMERO DE SEÑAL Y DESPUÉS HAY OTRO QUE PONDRÁ EL SISTEMA OPERATIVO
    print("Signal handler with signal: ", signum)
    print("hasta luego lucas!")
    sys.exit(0)

def nodeath(signum, frame):         # ARGUMENTO FRAME -> NUNCA LO UTILIZAMOS -> es un objeto compuesto
    print("Signal handler with sifgnal:, signum")
    print("Tururut! more't tu!")

# Assignar un handler al senyal
    
# SI LE PASAMOS LA SEÑAL 10 LO QUE HAREMOS ES QUE DISPARE LA FUNCIÓN myhandler, ESTO SIGNIFICA QUE PRINTARÁ LAS FRASES DE LA FUNCIÓN

signal.signal(signal.SIGUSR1,myhandler)       #10 -> LE ESTAMOS DICIENDO QUE CUANDO RECIBA LA SEÑAL SIGUSR1 -> EJECUTE LA FUNCIÓN "myhandler"

signal.signal(signal.SIGUSR2,nodeath)         #12

signal.signal(signal.SIGALRM,myhandler)       #14

signal.signal(signal.SIGTERM,signal.SIG_IGN)  #15

signal.signal(signal.SIGINT,signal.SIG_IGN)   #2

signal.alarm(60)           # SI NO HA PASADO NADA DURANTE ESE TIEMPO, MUESTRA LA FUNCIÓN "myhandler" 

print(os.getpid())          # PARA SABER EL PID DEL PROCESO


while True:     # HACE UN BUCLE INFINITO
    pass        # NO HACE NADA, PARA SOLO HACER EL EJEMPLO

sys.exit(0)



