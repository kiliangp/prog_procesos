#! /usr/bin/python3
#-*- coding: utf-8-*-
# Kilian Steven Guanoluisa Pionce
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] [-f file]
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="head.py",\
   epilog="Final del anàlisi!")

# choices -> Especifica que l'argument ha de ser o 5 o 10 o 15.
parser.add_argument("-n","--nlin", type=int,\
   dest="nlines", help="número de linies a processar (10)",\
   metavar="num_linies", \
   choices=[5, 10, 15], default=10)

parser.add_argument("-f", "--file", type=str, \
  dest="file", help="fitxer a processar (stdin)", metavar="nom_fitxer", \
  default="/dev/stdin")

args = parser.parse_args()
print(args)


#============= Programa ==============

MAXLIN=args.nlines

count=0

fileIn=open(args.file, "r")

for line in fileIn:
    count+=1
    print(line,end="")
    if count==MAXLIN:
        break
fileIn.close()

exit(0)


#============= Outputs ==============

# Sense cap argument 
   # ls -l / | python3 head_choice.py 
      # Namespace(nlines=10, file='/dev/stdin')
      # total 2923892
      # lrwxrwxrwx   1 root root          7 nov 27  2022 bin -> usr/bin
      # drwxr-xr-x   4 root root       4096 mar 20 08:56 boot
      # drwxrwxr-x   2 root root       4096 jul 11  2018 cdrom
      # -rw-------   1 root root  846442496 ene  6  2020 core
      # drwxr-xr-x  19 root root       5160 abr  4 21:29 dev
      # drwxr-xr-x 235 root root      20480 abr  4 16:48 etc
      # drwxr-xr-x   3 root root       4096 feb  7  2021 gnu
      # drwxr-xr-x   8 root root       4096 sep 20  2023 home
      # lrwxrwxrwx   1 root root         34 sep 22  2020 initrd.img -> boot/initrd.img-4.15.0-118-generic

# Amb el primer argument 
   #ls -l / | python3 head_choice.py -n 10
      # Namespace(nlines=10, file='/dev/stdin')
      # total 2923892
      # lrwxrwxrwx   1 root root          7 nov 27  2022 bin -> usr/bin
      # drwxr-xr-x   4 root root       4096 mar 20 08:56 boot
      # drwxrwxr-x   2 root root       4096 jul 11  2018 cdrom
      # -rw-------   1 root root  846442496 ene  6  2020 core
      # drwxr-xr-x  19 root root       5160 abr  4 21:29 dev
      # drwxr-xr-x 235 root root      20480 abr  4 16:48 etc
      # drwxr-xr-x   3 root root       4096 feb  7  2021 gnu
      # drwxr-xr-x   8 root root       4096 sep 20  2023 home
      # lrwxrwxrwx   1 root root         34 sep 22  2020 initrd.img -> boot/initrd.img-4.15.0-118-generic

# Amb els dos arguments
   # python3 head_choice.py -n 5 -f ../exemples/ex.1/dades.txt 
      # Namespace(nlines=5, file='../exemples/ex.1/dades.txt')
      # PRETTY_NAME="Debian GNU/Linux 12 (bookworm)"
      # NAME="Debian GNU/Linux"
      # VERSION_ID="12"
      # VERSION="12 (bookworm)"
      # VERSION_CODENAME=bookworm

# Amb el primer argument incorrecte
   # ls -l / | python3 head_choice.py -n 4
      # usage: head.py [-h] [-n num_linies] [-f nom_fitxer]
      # head.py: error: argument -n/--nlin: invalid choice: 4 (choose from 5, 10, 15)

# Amb els dos arguments pero amb el primer incorrecte
   # python3 head_choice.py -n 7 -f ../exemples/ex.1/dades.txt 
      # usage: head.py [-h] [-n num_linies] [-f nom_fitxer]
      # head.py: error: argument -n/--nlin: invalid choice: 7 (choose from 5, 10, 15)
