#! /usr/bin/python3
#-*- coding: utf-8-*-
# Kilian Steven Guanoluisa Pionce
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] file
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")

parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="5|10|15", choices=[5,10,15],default=10)

parser.add_argument("fitxer",type=str,\
        help="fitxer a processar", metavar="file")
args=parser.parse_args()
print(args)

# DIFERENCIA ENTRE PARÁMETRO POSICIONAL Y PARÁMETRO OPCIONAL -> EL NOMBRE

# -------------------------------------------------------

MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line, end='')
  if counter==MAXLIN: break
fileIn.close()
exit(0)


