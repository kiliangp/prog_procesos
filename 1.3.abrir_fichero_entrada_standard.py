#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Kilian Steven Guanoluisa Pionce
#
# ----------------

import sys
MAX=5
fileIn=sys.stdin       # POR DEFECTO LO QUE HAREMOS ES LEER EL FLUJO (ENTRADA STANDARD), EN CASO DE QUE EL USUARIO PRESENTE UN ARGUMENTO, LO LEEREMOS


if len(sys.argv) == 2:  # COMPROBACIÓN DE ARGUMENTOS
    
    fileIn = open(sys.argv[1],"r")
count = 0

for line in fileIn:
   count+=1
   print(line,end="")
   if count==MAX:
      break
fileIn.close()
####

    
exit(0) # SALIMOS DEL PROGRAMA
